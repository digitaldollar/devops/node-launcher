#!/usr/bin/env bash

set -e

source ./scripts/core.sh

get_node_info_short

echo "=> Pausing node global halt from a DDNode named $boldyellow$NAME$reset"
confirm

kubectl exec -it -n "$NAME" -c ddnode deploy/ddnode -- /kube-scripts/pause.sh
sleep 5
echo DIGITALDollar paused

display_status
