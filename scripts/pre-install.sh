#!/usr/bin/env bash

source ./scripts/core.sh

get_node_info

case $NET in
  mainnet)
    EXTRA_ARGS="-f ./ddnode-stack/chaosnet.yaml"
    ;;
  stagenet)
    EXTRA_ARGS="-f ./ddnode-stack/stagenet.yaml"
    ;;
  testnet)
    EXTRA_ARGS="-f ./ddnode-stack/testnet.yaml"
    ;;
esac

if node_exists; then
  warn "Found an existing DDNode, make sure this is the node you want to update"
  display_status
  echo
fi

echo -e "=> Deploying a $boldgreen$TYPE$reset DDNode on $boldgreen$NET$reset named $boldgreen$NAME$reset"
confirm

create_namespace
if [ "$TYPE" != "daemons" ]; then
  create_password
  create_mnemonic
fi
