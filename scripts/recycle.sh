#!/usr/bin/env bash

set -eo pipefail

source ./scripts/core.sh

get_node_info

if ! node_exists; then
  die "No existing DDNode found, make sure this is the correct name"
fi

if [ "$TYPE" != "validator" ]; then
  die "Only validators should be recycled"
fi

display_status

echo -e "=> Recycling a $boldgreen$TYPE$reset DDNode on $boldgreen$NET$reset named $boldgreen$NAME$reset"
echo
echo
warn "!!! Make sure your got your BOND back before recycling your DDNode !!!"
confirm

# delete gateway resources
echo -e "$beige=> Recycling DDNode - deleting gateway resources...$reset"
kubectl -n "$NAME" delete deployment gateway
kubectl -n "$NAME" delete service gateway
kubectl -n "$NAME" delete configmap gateway-external-ip

# delete ddnode resources
echo -e "$beige=> Recycling DDNode - deleting ddnode resources...$reset"
kubectl -n "$NAME" delete deployment ddnode
kubectl -n "$NAME" delete configmap ddnode-external-ip
kubectl -n "$NAME" delete secret ddnode-password
kubectl -n "$NAME" delete secret ddnode-mnemonic

# delete all key material from ddnode while preserving chain data
echo -e "$beige=> Recycling DDNode - deleting ddnode derived keys...$reset"
IMAGE=alpine:latest@sha256:4edbd2beb5f78b1014028f4fbb99f3237d9561100b6881aabbf5acce2c4f9454
SPEC=$(
  cat <<EOF
{
  "apiVersion": "v1",
  "spec": {
    "containers": [
      {
        "command": [
          "rm",
          "-rf",
          "/root/.ddnode/DIGITALDollar-ED25519",
          "/root/.ddnode/data/priv_validator_state.json",
          "/root/.ddnode/keyring-file/",
          "/root/.ddnode/config/node_key.json",
          "/root/.ddnode/config/priv_validator_key.json",
          "/root/.ddnode/config/genesis.json"
        ],
        "name": "reset-ddnode-keys",
        "stdin": true,
        "tty": true,
        "image": "$IMAGE",
        "volumeMounts": [{"mountPath": "/root", "name":"data"}]
      }
    ],
    "volumes": [{"name": "data", "persistentVolumeClaim": {"claimName": "ddnode"}}]
  }
}
EOF
)
kubectl -n "$NAME" run -it --rm reset-ddnode-keys --restart=Never --image="$IMAGE" --overrides="$SPEC"

# delete bifrost resources
echo -e "$beige=> Recycling DDNode - deleting bifrost resources...$reset"
kubectl -n "$NAME" delete deployment bifrost
kubectl -n "$NAME" delete pvc bifrost
kubectl -n "$NAME" delete configmap bifrost-external-ip

# recreate resources
echo -e "$green=> Recycling DDNode - recreating deleted resources...$reset"
NET=$NET TYPE=$TYPE NAME=$NAME ./scripts/install.sh

echo -e "$green=> Recycle complete.$reset"
