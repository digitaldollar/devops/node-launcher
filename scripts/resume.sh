#!/usr/bin/env bash

set -e

source ./scripts/core.sh

get_node_info_short

echo "=> Resuming node global halt from a DDNode named $boldyellow$NAME$reset"
confirm

kubectl exec -it -n "$NAME" -c ddnode deploy/ddnode -- /kube-scripts/resume.sh
sleep 5
echo DIGITALDollar resumed

display_status
