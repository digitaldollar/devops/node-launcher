#!/usr/bin/env bash

set -e

source ./scripts/core.sh

get_node_info_short

echo "=> Setting DDNode IP address"
kubectl exec -it -n "$NAME" deploy/ddnode -- /kube-scripts/set-ip-address.sh "$(kubectl -n "$NAME" get configmap gateway-external-ip -o jsonpath="{.data.externalIP}")"
sleep 5
echo DDNode IP address updated
