#!/usr/bin/env bash

set -e

source ./scripts/core.sh

get_node_info_short

echo "=> Setting DDNode keys"
kubectl exec -it -n "$NAME" -c ddnode deploy/ddnode -- /kube-scripts/set-node-keys.sh
sleep 5
echo DDNode Keys updated
