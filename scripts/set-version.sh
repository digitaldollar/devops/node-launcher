#!/usr/bin/env bash

set -e

source ./scripts/core.sh

get_node_info_short

echo "=> Setting DDNode version"
kubectl exec -it -n "$NAME" -c ddnode deploy/ddnode -- /kube-scripts/retry.sh /kube-scripts/set-version.sh
sleep 5
echo DDNode version updated

display_status
